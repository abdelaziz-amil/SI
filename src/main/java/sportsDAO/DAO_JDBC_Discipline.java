package sportsDAO;

import donnees.Discipline;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * DAO pour la classe/table Discipline avec implémentation en JDBC.
 * @author Eric
 */
class DAO_JDBC_Discipline extends DAO<Discipline> {

    private Connection connection = null;
    
    @Override
    public Discipline find(int id) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void create(Discipline disc) throws DAOException {
        try {
            Statement req = connection.createStatement();
            ResultSet res = req.executeQuery("select max(code_discipline) from discipline");
            res.next();
            int codeDisc = res.getInt(1);
            codeDisc++;
            disc.setCodeDiscipline(codeDisc);

            req = connection.createStatement();
            int nb = req.executeUpdate("insert into discipline values (" + codeDisc + " , '" + disc.getIntitule() + "' , " + disc.getSport().getCodeSport() + ")");
        } catch (Exception e) {
            throw new DAOException("Problème technique (" + e.getMessage() + ")");
        }
    }

    @Override
    public void update(Discipline data) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Discipline data) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public DAO_JDBC_Discipline() throws DAOException {
    	super();
        this.connection = SQLConnection.getConnection();
    }
    
}
